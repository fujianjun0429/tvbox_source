package com.github.tvbox.osc.bean;

import java.util.HashMap;
import java.util.List;

public class SiteMuti {
    public List<MSite> urls;

    public class MSite{
        public String url;
        public String name;
    }
}